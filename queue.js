let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    let newCollection = [];

    for(let i = 1; i <= collection.length - 1; i++) {
        newCollection[i-1] = collection[i];
    };
    collection = newCollection;
    return collection;
}

function front() {
   return collection[0];
}

function size() {
    let count = 0;

    for(let i = 1; i <= collection.length; i++) {
        count++;
    };

    return count++;
}

function isEmpty() {
    let empty;
    (collection.length > 0)
    ? empty = false
    : empty = true;

    return empty;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
